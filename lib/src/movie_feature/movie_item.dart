/// A placeholder class that represents an entity or model.
class MovieItem {
  const MovieItem(
      {required this.name, required this.popularity, this.imagePath = ''});

  final String name;
  final double popularity;
  final String imagePath;
}

class MovieList {
  final List<MovieItem> movies;

  MovieList({required this.movies});

  factory MovieList.fromJson(Map<String, dynamic> json) {
    var results = json['results'] as List;
    List<MovieItem> movieList = results.map((i) {
      return MovieItem(
        name: i['original_title'],
        popularity: i['popularity'].toDouble(),
        imagePath: i['poster_path'],
      );
    }).toList();

    return MovieList(movies: movieList);
  }
}
