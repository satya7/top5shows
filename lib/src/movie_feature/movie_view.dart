import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:top5shows/src/movie_feature/movie_items_builder.dart';
import 'package:unleash_client/unleash_client.dart';

import '../settings/settings_view.dart';
import 'movie_item.dart';

const staticMoviesList = [
  MovieItem(name: 'Fast X', popularity: 3583.861),
  MovieItem(name: 'Barbie', popularity: 2929.987),
  MovieItem(name: 'The retribution', popularity: 2685.799),
  MovieItem(name: 'Meg 2: The Trench', popularity: 1608.611),
  MovieItem(name: 'Elemental', popularity: 1241.972),
];
final Map<String, String> regionToCode = {
  'USA': 'US',
  'India': 'IN',
  'Canada': 'CA',
  'UK': 'GB',
};
String apiKey = "13051bf3625e24b87272342829295fa2";
bool _showGridView = false;

// ignore: must_be_immutable
class MovieListView extends StatefulWidget {
  MovieListView({
    Key? key,
    required this.items,
  }) : super(key: key);

  static const routeName = '/';

  List<MovieItem> items = staticMoviesList;
  String? selectedRegion; //new code

  @override
  // ignore: library_private_types_in_public_api
  _MovieListViewState createState() => _MovieListViewState();
}

class _MovieListViewState extends State<MovieListView> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) async {
    if (index == 0) {
      widget.items = staticMoviesList;
    } else if (index == 1) {
      await _fetchMoviesFromAPI();
    } else if (index == 2) {
      await _fetchFeatureFlagMoviesAndToggle();
      //New code
    } else if (index == 3) {
      await _fetchMoviesFromAPIWithRegion();
    }
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<void> _fetchMoviesFromAPI() async {
    var request = http.Request(
        'GET',
        Uri.parse(
            'https://api.themoviedb.org/3/movie/popular?api_key=$apiKey'));
    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      final String body = await response.stream.bytesToString();
      if (kDebugMode) {
        print(body);
      }

      final movieList = MovieList.fromJson(jsonDecode(body));
      widget.items = movieList.movies;
    } else {
      if (kDebugMode) {
        print(response.reasonPhrase);
      }
    }
  }

  Future<void> _fetchMoviesFromAPIWithRegion() async {
    String regionCode = regionToCode[widget.selectedRegion] ?? 'US';

    var request = http.Request(
        'GET',
        Uri.parse(
            'https://api.themoviedb.org/3/movie/popular?api_key=$apiKey&region=$regionCode')); // Add the region parameter to the API request
    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      final String body = await response.stream.bytesToString();
      if (kDebugMode) {
        print(body);
      }

      final movieList = MovieList.fromJson(jsonDecode(body));
      widget.items = movieList.movies;
    } else {
      if (kDebugMode) {
        print(response.reasonPhrase);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Top 5 movies'),
        actions: [
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () {
              Navigator.restorablePushNamed(context, SettingsView.routeName);
            },
          ),
        ],
      ),
      body: Column(
        children: [
          const SizedBox(height: 16),
          const Text('Code created time: 2023-09-16 22:00'),
          Text(
            ('Current date and time: ${DateFormat('yyyy-MM-dd HH:mm').format(DateTime.now())}'),
          ),
          // Code added for code push
          if (_selectedIndex == 3)
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: DropdownButton<String>(
                isExpanded: true, // To take full width
                value: widget.selectedRegion,
                items: ['USA', 'India', 'Canada', 'UK'].map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (newValue) {
                  setState(() {
                    _fetchMoviesFromAPIWithRegion();
                    widget.selectedRegion = newValue;
                  });
                },
                hint: const Text('Select Region'),
              ),
            ),
          Expanded(
              child: MovieItemsBuilder(
                  items: widget.items, isGridView: _showGridView)),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.block),
            label: 'Static',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.api),
            label: 'API',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.flag),
            label: 'Feature Flags',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.dynamic_feed),
            label: 'Codepush',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }

  _fetchFeatureFlagMoviesAndToggle() async {
    final unleash = await Unleash.init(
      UnleashSettings(
        appName: 'production',
        instanceId: 'dbQQzDN2gs1Xk1ShZcxB',
        unleashApi: Uri.parse(
            'https://gitlab.com/api/v4/feature_flags/unleash/50449121'),
        apiToken: '',
      ),
    );
    _showGridView = unleash.isEnabled('show_grid_view');
  }
}
