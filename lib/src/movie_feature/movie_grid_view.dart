import 'package:flutter/material.dart';
import 'package:top5shows/src/movie_feature/movie_item.dart';
import 'package:top5shows/src/movie_feature/movie_item_details_view.dart';

class MovieGridView extends StatelessWidget {
  const MovieGridView({
    super.key,
    required this.items,
  });

  final List<MovieItem> items;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        childAspectRatio: 0.7,
      ),
      itemCount: 5,
      itemBuilder: (BuildContext context, int index) {
        final item = items[index];

        return GestureDetector(
          onTap: () {
            Navigator.restorablePushNamed(
              context,
              MovieDetailsView.routeName,
            );
          },
          child: Stack(
            alignment: Alignment.bottomCenter, // Align children at the bottom
            children: [
              // Image
              item.imagePath != ''
                  ? Image.network(
                      'http://image.tmdb.org/t/p/w185${item.imagePath}',
                      fit: BoxFit.cover, // Cover the entire grid item
                    )
                  : Image.asset('assets/images/flutter_logo.png',
                      fit: BoxFit.cover),

              // Movie name
              Container(
                color: Colors.black.withOpacity(0.6), // Semi-transparent black
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text(
                    item.name,
                    style: const TextStyle(color: Colors.white),
                    overflow: TextOverflow.ellipsis, // Ellipsis for long names
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
