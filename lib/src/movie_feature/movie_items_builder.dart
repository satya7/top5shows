import 'package:flutter/material.dart';
import 'package:top5shows/src/movie_feature/movie_grid_view.dart';
import 'package:top5shows/src/movie_feature/movie_item.dart';
import 'package:top5shows/src/movie_feature/movie_list_view.dart';

class MovieItemsBuilder extends StatelessWidget {
  const MovieItemsBuilder(
      {super.key, required this.items, required this.isGridView});

  final List<MovieItem> items;
  final bool isGridView;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: isGridView
          ? MovieGridView(items: items)
          : MovieListView(items: items),
    );
  }
}
