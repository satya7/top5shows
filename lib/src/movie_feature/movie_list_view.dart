import 'package:flutter/material.dart';
import 'package:top5shows/src/movie_feature/movie_item.dart';
import 'package:top5shows/src/movie_feature/movie_item_details_view.dart';

class MovieListView extends StatelessWidget {
  const MovieListView({
    super.key,
    required this.items,
  });

  final List<MovieItem> items;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      // Providing a restorationId allows the ListView to restore the
      // scroll position when a user leaves and returns to the app after it
      // has been killed while running in the background.
      restorationId: 'MovieItemListView',
      itemCount: 5,
      itemBuilder: (BuildContext context, int index) {
        final item = items[index];

        return ListTile(
            title: Text('${item.name}, ${item.popularity}'),
            leading: item.imagePath != ''
                ? Image.network(
                    'http://image.tmdb.org/t/p/w185${item.imagePath}')
                : const CircleAvatar(
                    foregroundImage:
                        AssetImage('assets/images/flutter_logo.png'),
                  ),
            onTap: () {
              // Navigate to the details page. If the user leaves and returns to
              // the app after it has been killed while running in the
              // background, the navigation stack is restored.
              Navigator.restorablePushNamed(
                context,
                MovieDetailsView.routeName,
              );
            });
      },
    );
  }
}
