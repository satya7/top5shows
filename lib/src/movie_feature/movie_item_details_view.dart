import 'package:flutter/material.dart';

/// Displays detailed information about a SampleItem.
class MovieDetailsView extends StatelessWidget {
  const MovieDetailsView({super.key});

  static const routeName = '/movie_details';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Movie Details'),
      ),
      body: const Center(
        child: Text('More Information Here'),
      ),
    );
  }
}
